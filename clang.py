import subprocess
import re

class ClangComplete:
	'''
	Clang completion service based on clang compiler.
	It uses the project configurations and the actual buffer
	to parse the file and provide a list of sugestions to the user
	'''

	completion_re = re.compile(r"COMPLETION: ")

	def __init__(self, includes = ""):
		self.__includes = includes

	def get_completions(self, buf, locations):
		'''
		main method called by the EventListener implementation.
		this method is responsable by dispatching the clang subprocess
		and parsing it's sugestions.
		'''
		completions_unparsed = self._call_tool(buf, locations)
		completions = self._prepare_completions(completions_unparsed)
		return completions
		

	def _call_tool(self, buf, locations):
		for location in locations:
			line, column = self._get_line_column(buf, location)

			#"-I%s" % self.__includes,
			command = ["clang", "-cc1", "-fsyntax-only", "-code-completion-at=-:%d:%d" % (line, column)]
			clang = subprocess.Popen(command, stdout = subprocess.PIPE, stdin = subprocess.PIPE, stderr = subprocess.PIPE)

			clang.stdin.write(buf)
			clang.stdin.flush()
			clang.stdin.close()
			completions_unparsed = clang.stdout.read()
			completions_unparsed = completions_unparsed.split('\n')

		return completions_unparsed

	def _prepare_completions(self, completions_unparsed):
		completions = []
		for proposed_completion in completions_unparsed:
			if ClangComplete.completion_re.match(proposed_completion):
				completion_splited = proposed_completion.split(":")
				'''TODO clang provides a snippet complete too, we could use this to improve the plugin'''
				completions.append(completion_splited[1].strip())

		return completions

	def _get_line_column(self, buf, location):
		_buf = buf.split('\n')
		count = -1
		for line in _buf:
			count = count + 1
			if location <= len(line):
				return count, location
			else:
				location = location - len(line)

		return count, len(line)
