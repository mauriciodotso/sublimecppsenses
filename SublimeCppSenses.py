import sublime, sublime_plugin
import clang

class SublimeCppSensesCommand(sublime_plugin.EventListener):
	
	def __init__(self):
		self.clang_complete = clang.ClangComplete()

	def on_query_completions(self, view, prefix, locations):
		if "C++" in view.settings().get("syntax"):
			region = sublime.Region(0, view.size())
			buf = view.substr(region)
			completions = self.clang_complete.get_completions(buf, locations)
			completions = [(item, item) for item in completions]
			return completions
